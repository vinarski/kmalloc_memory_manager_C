#include "kmalloc.h"
#include <iostream> //debug (cout)

//for each call of new of EVERY class
//this overloaded operator new will execute
//instead of the standard library one
//if you want to limit this behaviour
//to a specific class you can overload that
//calss' operator new only
/////overload the global operator new
void *operator new (size_t size)
{
    void *ptr = kmalloc (size);
    std::cout << "NEW! ptr:" << ptr << "\n"; //debug
    return ptr;
}

//overload the global operator delete
void operator delete (void *ptr)
{
    std::cout << "DELETE! ptr: " << ptr << "\n"; //debug
    kfree (ptr);
}

//overload the global operator new []
void *operator new [] (size_t size)
{
    std::cout << "operator new [] called with size: " << size << "\n"; //debug
    return operator new (size);
}

//overload the global operator delete []
void operator delete [] (void *ptr)
{
    std::cout << "operator delete [] called" << "\n"; //debug
    std::cout << "...with pointer: " << ptr << "\n"; //debug
    operator delete (ptr);
}
