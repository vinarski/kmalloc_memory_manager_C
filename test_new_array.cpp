#include <iostream>
#include <stdlib.h>
#include "kmalloc.h" //debug functions

class A
{
    private:
    int i;
    public:
    A() {std::cout << "CTOR A(), this: " << this << "\n";}
    ~A() {std::cout << "DTOR A(), this: " << this << "\n";}
};

int main (void)
{
    std::cout << "sizeof int: " << sizeof (int) << "\n";
    std::cout << "sizeof A: " << sizeof (A) << "\n";
    std::cout << "sizeof array A[3] is size of A (4) times num of elements (3): "
         << sizeof (A) * 3 << "\n";

    //new calculates the size of the array
    //and adds 8 for the header
    //it then calls operator new [] with this size
    //operator new [] just calls operator new
    //After the memory is allocated new iterates over the array
    //calling the default (!) constructor for each element
    A *a = new A[3];
    std::cout << "array pointer: " << a << "\n";

    //delete calls operator delete [] with the pointer (a - 8).
    //a-8 contains the number of the elements header
    //a is the pointer to the first element of the array
    //before deletion - check the contents of the array header
    //cout << "array header at pointer a-8: " << (size_t)(((size_t)a) - 1) << endl;
    std::cout << "array header at pointer a-8: " << *((size_t *)a - 1) << "\n";
    delete[] a;

    return 0;
}
