#include <stdio.h>
#include "kmalloc.h"

typedef struct
{
    int arr[10000];

} test_block_t;

typedef struct
{
    int arr[5000];

} test_little_block_t;


#if 0
int main (void)
{
    int x = 7;
    int y = 8;
    int z = 9;
    int X = 2*MB - 100;
    int Y = 2*MB;
    int Z = 2*MB + 11;

    printf ("x: %d y: %d z: %d\n", EXTEND_TO_BLOCK_SIZE(x),EXTEND_TO_BLOCK_SIZE(y),EXTEND_TO_BLOCK_SIZE(z));
    printf ("X: %d Y: %d Z: %d\n", EXTEND_TO_SBRK_BLOCK_SIZE(X),EXTEND_TO_SBRK_BLOCK_SIZE(Y),EXTEND_TO_SBRK_BLOCK_SIZE(Z));

    return 0;
}
#endif

#if 0
int main (void)
{
    char *pointer = (char *)kmalloc (1000);
    kfree (pointer);

    return 0;
}
#endif

#if 1
int main (void)
{
    test_block_t *arr[100];
    int i;

    for (i = 0; i < 100; i ++)
    {
        arr[i] = kmalloc (sizeof (test_block_t));
        //printf ("size: %lu\n", get_block_size (arr[i]));
        //kmalloc_debug_available_blocks ();
    }

    debug_available_mem_blocks ();
    printf ("First Step End. Press any key.\n");   
    getchar ();

    for (i = 0; i < 100; i ++)
    {
        arr[i] = krealloc (arr[i], sizeof (test_little_block_t));
        //printf ("Free blocks: %d\n", kmalloc_get_num_free_blocks ());
        //getchar ();        
        //kmalloc_debug_available_blocks ();
    }
    debug_available_mem_blocks ();
    printf ("Second Step End. Free Blocks: %d. Press any key.\n",
            debug_get_num_free_blocks ());
    getchar ();

    for (i = 0; i < 100; i ++)
    {
        kfree (arr[i]);
        //printf ("Free blocks: %d\n", debug_get_num_free_blocks ());
        //getchar ();       
        //kmalloc_debug_available_blocks ();
    }

    printf ("Last step.\n");
    debug_available_mem_blocks ();
    printf ("Finish.\n");

    printf ("Free blocks: %d\n", debug_get_num_free_blocks ());

    return 0;
}
#endif

#if 0
int main (void)
{
    void* p1 = kmalloc (101);
    void* p2 = kmalloc (199);
    void* p3 = kmalloc (300);
    void* p4 = kmalloc (400);
    
    printf ("-------- 4 memory blocks allocated ----------\n");
    kmalloc_debug_available_blocks ();
    
    kfree (p1);
    printf ("-------- p1 released ----------\n");
    kmalloc_debug_available_blocks ();
    kfree (p3);
    printf ("-------- p3 released ----------\n");
    kmalloc_debug_available_blocks ();
    kfree (p4);
    printf ("-------- p4 released ----------\n");
    kmalloc_debug_available_blocks ();
    kfree (p2);
    printf ("-------- p2 released ----------\n");
    kmalloc_debug_available_blocks ();

    return 0;
}
#endif


