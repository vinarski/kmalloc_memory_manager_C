#include <string>
#include <vector>
#include <iostream>
#include <memory> //shared_ptr<T>
#include "kmalloc.h" //debug functions

class TestPerson
{
private:
    int age;
    std::string name;
public:
    //CTOR
    TestPerson (int age, std::string name)
    :age (age), name (name)
    {}

    int getAge () {return age;}
    std::string getName () {return name;}

};

int main (void)
{
    std::shared_ptr<TestPerson> p1 (new TestPerson (3, "Nora"));
    std::cout << "Person age: " << p1->getAge ()
        << " Person name: " << p1->getName () << "\n";

    std::cout << " ----------- DEBUG ----------- " << "\n";
    std::cout << "Number of available memory blocks: "
              << debug_get_num_free_blocks () << "\n";
    std::cout << "List of available memory blocks: " << "\n";
    debug_available_mem_blocks ();

    {
        std::vector< std::shared_ptr<TestPerson> > people;
        std::cout << "Before reserve " << "\n";
        people.reserve (10); //init capacity is 10
        std::cout << "After reserve " << "\n";

        for (int i = 0; i < 10; i++)
        {
            std::cout << "i: " << i << "\n";
            people.push_back (std::shared_ptr<TestPerson> (new TestPerson (i,
                (std::string("Nora") + std::to_string (i)))));
        }

        for (int i = 0; i < 10; i+=2)
        {
            std::cout << "reset i: " << i << "\n";
            people[i].reset ();
        }

        std::cout << " ----------- DEBUG ----------- \n";
        std::cout << "Number of available memory blocks: "
                  << debug_get_num_free_blocks () << "\n";
        std::cout << "List of available memory blocks: " << "\n";
        debug_available_mem_blocks ();
    }

    std::cout << " ----------- DEBUG ----------- \n";
    std::cout << "Number of available memory blocks: "
              << debug_get_num_free_blocks () << "\n";
    std::cout << "List of available memory blocks: \n";
    debug_available_mem_blocks ();

    return 0;
}
