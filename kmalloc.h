#ifndef KMALLOC_H_
#define KMALLOC_H_

#define MAX(x_, y_) ((x_) > (y_) ? (x_) : (y_))
#define MEGABYTE (1024*1024)
#define SBRK_BLOCK_SIZE MEGABYTE
#define KMALLOC_BLOCK_SIZE 8
#define POINTER_SIZE 8
#define SIZE_OF_SIZE_T 8

//16 bytes can accommodate the node header
//returned to the user there is 8 bytes for the header
//and 8 bytes for the payload
#define KMALLOC_MIN_FREEBLOCK_SIZE 16

//this macro rounds up (extends) the block size to make it divisible by 8 (bytes)
//3 bits is 2³ = 8
#define EXTEND_TO_BLOCK_SIZE(x_) ( (((x_) + KMALLOC_BLOCK_SIZE - 1) >> 3) << 3 )

//this macro rounds up (extends) the block size to make it divisible by MEGABYTE
//20 bits is 2²⁰ = 1024*1024 (MEGABYTE)
#define EXTEND_TO_SBRK_BLOCK_SIZE(x_) ( (((x_) + SBRK_BLOCK_SIZE - 1) >> 20) << 20 )

#include <stdlib.h>

typedef struct free_block_head_tag
{
	size_t size;
	struct free_block_head_tag *next;

} free_block_head_t;

//list descriptor
typedef struct kmalloc_tag
{
    free_block_head_t *first;

} kmalloc_t;

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */

void debug_available_mem_blocks (void);

int debug_get_num_free_blocks (void);

size_t debug_get_block_size (void *ptr);

void *kmalloc (size_t size);

void kfree (void *ptr);

void *krealloc (void *ptr, size_t new_size);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */


#endif //KMALLOC_H_
