#include <unistd.h>
#include <stdio.h>
#include <string.h>

#include "kmalloc.h"

//there is no separate implementation of the linked list
//instead the linked list is built into the functions
//this makes the functions bulky but efficient
//efficiency is important since
//this functions are performance critical
static kmalloc_t kmalloc_manager; //static makes this global variable file private
//first is a pointer, so it is a scalar value which will be automatically initialized to 0

/* DEBUG functions */
void debug_available_mem_blocks (void)
{
    free_block_head_t *current;

    for (current = kmalloc_manager.first; current != NULL; current = current->next)
    {
        printf ("Current block address: %p size %lu: next block address: %p\n", 
                current, current->size, current->next);
    }
    printf ("=========================================\n");
}

int debug_get_num_free_blocks (void)
{
    int num = 0;
    free_block_head_t *current;

    for (current = kmalloc_manager.first; current != NULL;
         current = current->next, ++ num) 
    {}

    return num;
}

size_t debug_get_block_size (void *ptr)
{
    unsigned char *blockRaw = ((unsigned char *)ptr) - SIZE_OF_SIZE_T;
    free_block_head_t *block = (free_block_head_t *)blockRaw;

    return block->size;
}
/* end of DEBUG functions */

void kfree (void *ptr)
{
    unsigned char *newNodeRaw = ((unsigned char *)ptr) - SIZE_OF_SIZE_T; 
    free_block_head_t *newNode = (free_block_head_t *)newNodeRaw;

    free_block_head_t *current;
    free_block_head_t *prev = NULL; //previous node

    for (current = kmalloc_manager.first; current != NULL; current = current->next)
    {   
        unsigned char *currentRaw = (unsigned char *)current;

        // newNode is left from all nodes
        if (current > newNode && prev == NULL) //current is head and is greater than the newNode
        {   
            kmalloc_manager.first = newNode; //the newNode becomes head
            
            if ((newNodeRaw + SIZE_OF_SIZE_T + newNode->size) == currentRaw) //possible to merge
            {
                newNode->size += current->size + SIZE_OF_SIZE_T;
                newNode->next = current->next;
            }
            else
                newNode->next = current;
            
            return;
        }
        
        // newNode is right from all nodes
        if (current->next == NULL) //tail!
        {
            if ((currentRaw + SIZE_OF_SIZE_T + current->size) == newNodeRaw) //possible to merge
                current->size += newNode->size + SIZE_OF_SIZE_T; //current remains tail
            else
            {
                current->next = newNode; 
                newNode->next = NULL; //newNode becomes tail
            }

            return;
        }

        // newNode needs to be inserted between current and next
        if (current->next > newNode)
        {
            //possible to merge middle (the newNode) with the right (next node)
            if ((newNodeRaw + SIZE_OF_SIZE_T + newNode->size) == ((unsigned char *)current->next))
            {
                newNode->size += current->next->size + SIZE_OF_SIZE_T;
                newNode->next = current->next->next;
            }
            else 
                newNode->next = current->next;
        
            //possible to merge left (current) with the newNode
            if ((currentRaw + SIZE_OF_SIZE_T + current->size) == newNodeRaw) 
            {
                current->size += newNode->size + SIZE_OF_SIZE_T;
                current->next = newNode->next;
            }
            else //NOT possible to merge left (current) with the newNode
                current->next = newNode;

            return;
        }
        
        // newNode > current && newNode > current->next
        // continue iteration

        //save the current node's address
        prev = current;
    }
    
    //the for loop was skept means that the list is empty
    //add the head node to the list
    kmalloc_manager.first = newNode;
    //add the pointer-to-the-next-node-part of the header
    newNode->next = NULL;
}

void *kmalloc (size_t bytes)
{
    size_t payload_size = EXTEND_TO_BLOCK_SIZE (bytes);

    //the while loop allows the "jump back" and repeat the procedure
    //in case the matching memory block could not be found within the
    //free blocks list, so a new chunk of memory has been fetched from the 
    //operating system via sbrk and added to the free blocks list
    while (1)
    {
        //iterate over the free memory blocks list
        //the previous node's pointer needs to be saved on 
        //each iteration in order to be able to remove the current node
        //from the free blocks list if it matches

        size_t fresh_memory_block_size;
        free_block_head_t *fresh_memory_block;

        free_block_head_t *current;
        free_block_head_t *prev = NULL; //previous node

        for (current = kmalloc_manager.first; current != NULL; current = current->next)
        {
            unsigned char *currentRaw = (unsigned char *)current;

            if (current->size >= payload_size) //found!
            {
                //this node fits. Two scenarios possible: 
                //1. rare: the memory block size is just right - no need to split it
                //2. more common: the memory block is too big -> split it
                size_t split_size = current->size - payload_size;

                if (split_size < KMALLOC_MIN_FREEBLOCK_SIZE) //no splitting
                {
                    if (prev == NULL) //current is head
                        kmalloc_manager.first = current->next;
                    else
                        prev->next = current->next;

                    return currentRaw + SIZE_OF_SIZE_T; //points to just behind the size 
                             //to the payload - address of the current->next. The current->next pointer 
                             //information is no longer needed so it is part (beginning) of the payload
                             //The user receives the pointer to the payload, but just before this a
                             //SIZE_OF_SIZE_T bytes header is hidden containing the size of the payload.
                             //This size information is needed for the later kfree() call
                }
                else //needs a split!
                {
                    //the header of the second compartment
                    free_block_head_t *second_half = (free_block_head_t *)
                        (currentRaw + SIZE_OF_SIZE_T + payload_size);

                    second_half->size = current->size - payload_size - SIZE_OF_SIZE_T; 
                    second_half->next = current->next;
                    
                    //the header of the first compartment
                    current->size = payload_size;

                    //the previous node needs to be linked to the new splitted node (second compartment)
                    if (prev != NULL)
                        prev->next = second_half;
                    else
                        kmalloc_manager.first = second_half;
        
                    return currentRaw + SIZE_OF_SIZE_T; //return the first compartment
                }
            }
            
            //the current node's memory block size is too small
            //save the current node's address for reference on the next iteration
            prev = current;
        }
        
        //the for loop was skept or finished with no return, so no matching memory block
        //could not be found within the free blocks list or the list is empty
        fresh_memory_block_size = EXTEND_TO_SBRK_BLOCK_SIZE (payload_size + SIZE_OF_SIZE_T);
        fresh_memory_block = (free_block_head_t *)sbrk (fresh_memory_block_size);
        
        //set the payload size. free will need this information
        fresh_memory_block->size = fresh_memory_block_size - SIZE_OF_SIZE_T; 
        
        //call kfree in order to add the new memory block to the available free memory block list
        //The kfree receives the pointer to the payload (as if the user would call kfree),
        //but just before this memory location SIZE_OF_SIZE_T bytes header is hidden
        //containing the size of the payload.
        kfree (((unsigned char *)fresh_memory_block) + SIZE_OF_SIZE_T);
    }
}

void *krealloc (void *ptr, size_t new_size)
{
    unsigned char *currMemBlockRaw = ((unsigned char *)ptr) - SIZE_OF_SIZE_T; 
    free_block_head_t *currMemBlock = (free_block_head_t *)currMemBlockRaw;
    
    new_size = EXTEND_TO_BLOCK_SIZE (new_size);

    //new requested size equals old size - return same pointer
    if (new_size == currMemBlock->size)
        return ptr;
    
    //new requested size is less than old size - split and return same pointer
    if (new_size < currMemBlock->size)
    {
        //no splitting
        if ((currMemBlock->size - new_size) < KMALLOC_MIN_FREEBLOCK_SIZE)
            return ptr;
        else //split!
        {
            free_block_head_t *newMemBlock =
                (free_block_head_t *) (currMemBlockRaw + SIZE_OF_SIZE_T +
                                       new_size);

            newMemBlock->size = currMemBlock->size - new_size - SIZE_OF_SIZE_T;
            currMemBlock->size = new_size;
            kfree (((unsigned char *)newMemBlock) + SIZE_OF_SIZE_T);
            return ptr;
        }
    }

    //new requested size is greater than old size - get new memory block
    //and copy memory from old one into it
    else
    {
        void *newPtr = kmalloc (new_size);
        if (newPtr == NULL)
            return NULL;

        memcpy (newPtr, ptr, currMemBlock->size);
        kfree (ptr);

        return newPtr;
    }
}




